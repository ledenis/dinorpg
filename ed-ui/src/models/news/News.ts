import { Image } from './Image';

export interface News {
	title: string;
	image: Image;
	text: string;
	hide: boolean;
}
