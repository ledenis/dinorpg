export interface Image {
	data: Array<number>;
	type: string;
}
