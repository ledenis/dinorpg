import { Dinoz } from './dinoz';

export interface CommonData {
	money: number;
	dinoz: Dinoz;
	dinozCount: number;
	playerId: number;
}
