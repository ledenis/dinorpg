export interface PlayerRanking {
	dinozCount: number;
	pointCount: number;
	playerName: string;
	playerId: number;
	pointAverage: number;
	position: number;
}
