export enum ItemType {
	CLASSIC = 'classic',
	CURSED = 'cursed',
	MAGIC = 'magic'
}
