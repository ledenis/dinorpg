export interface NewPositions {
	playerId: number;
	sumPosition: number;
	averagePosition: number;
	sumPointsDisplayed: number;
	averagePointsDisplayed: number;
	dinozCountDisplayed: number;
}
