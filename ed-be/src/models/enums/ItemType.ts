export enum ItemType {
	CLASSIC = 'classic',
	MAGICAL = 'magical',
	CURSED = 'cursed'
}
