export * from './ingredient.js';
export * from './item.js';
export * from './level.js';
export * from './place.js';
export * from './race.js';
export * from './reward.js';
// Note: item.js and place.js are before shop.js because shop.js needs them
export * from './shop.js';
export * from './skill.js';
export * from './status.js';
export * from './action.js';

export const apiRoutes = {
	adminRoute: '/api/admin',
	dinozRoute: '/api/dinoz',
	ingredientRoute: '/api/ingredients',
	inventoryRoute: '/api/inventory',
	levelRoute: '/api/level',
	newsRoute: '/api/news',
	oauthRoute: '/api/oauth',
	playerRoute: '/api/player',
	rankingRoutes: '/api/ranking',
	shopRoutes: '/api/shop'
};

export const regex = {
	DINOZ_NAME: /^[a-zA-Z0-9éèêëÉÈÊËîïÎÏôÔûÛ\-']{3,16}$/
};
