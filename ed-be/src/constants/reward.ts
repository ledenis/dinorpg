export const rewardList: Record<string, number> = {
	TROPHEE_ROCKY: 10,
	TROPHEE_PTEROZ: 20,
	TROPHEE_HIPPOCLAMP: 30,
	TROPHEE_QUETZU: 40,
	PLUME: 13
};
